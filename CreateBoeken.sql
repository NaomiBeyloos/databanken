-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Genereertijd: 16 feb 2018 om 14:31
-- Serverversie: 5.5.57-0ubuntu0.14.04.1
-- PHP-versie: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `ModernWays`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Boeken`
--

CREATE TABLE IF NOT EXISTS `Boeken` (
  `Voornaam` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Familienaam` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `Titel` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Stad` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Verschijningsjaar` char(4) DEFAULT NULL,
  `Uitgeverij` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `Herdruk` char(4) DEFAULT NULL,
  `Commentaar` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
