-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Genereertijd: 16 feb 2018 om 15:01
-- Serverversie: 5.5.57-0ubuntu0.14.04.1
-- PHP-versie: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `ModernWays`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `Postcodes`
--

CREATE TABLE IF NOT EXISTS `Postcodes` (
  `Code` char(4) DEFAULT NULL,
  `Plaats` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Localite` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Provincie` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Province` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
