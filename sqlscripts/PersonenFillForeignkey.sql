-- NB
-- 23 mei 2018
-- bestandsnaam PersonenFillForeignkey.sql


use ModernWays;
update Personen, Aanspreektitel
set IdAanspreektitel = (select Aanspreektitel.Id
       from Aanspreektitel where Aanspreektitel.Description = Personen.Aanspreektitel);

select * from Personen