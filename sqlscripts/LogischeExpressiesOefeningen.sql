1. Bewaar de oefeningen in LogischeExpressiesOefeningen.sql;

2. Selecteer de boeken van twee verschillende auteurs (de namen kies jezelf);

use ModernWays;

select Voornaam, Familienaam, Titel 
	from Boeken
		where (Familienaam = 'Gowers' and Voornaam = 'Timothy')
		or  (Familienaam = 'Braudel' and Voornaam = 'Fernand');

3. Selecteer alle boeken die verschillen van één bepaalde auteur, m.a.w. alle boeken behalve van één auteur (de naam kies jezelf);

use ModernWays;

select Voornaam, Familienaam, Titel 
	from Boeken
			where not (Familienaam = 'Bly');

4. Insert Hugo Claus, De verwondering, Antwerpen, Manteau, 1970;

use ModernWays;
insert into Boeken(
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   InsertedBy
)
values (
   'Hugo',
   'Claus',
   'De Verwondering',
   'Antwerpen',
   'Manteau',
   '1970',
   'Naomi Beyloos'
);

5. Insert Hugo Raes, Jagen en gejaagd worden, Antwerpen, De Bezige Bij, 1954;

use ModernWays;
insert into Boeken(
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   InsertedBy
)
values (
   'Hugo',
   'Raes',
   'Jagen en Gejaagd worden',
   'Antwerpen',
   'De Bezige Bij',
   '1954',
   'Naomi Beyloos'
);

6. Insert Jean-Paul Sarthe, Het zijn en het niets, 1943, Parijs, Gallimard;

use ModernWays;
insert into Boeken(
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   InsertedBy
)
values (
   'Jean-Paul',
   'Sarthe',
   'Het Zijn en het Niets',
   'Parijs',
   'Gallimard',
   '1943',
   'Naomi Beyloos'
);

7. Selecteer alle boeken van de auteurs die de voornaam Hugo of Jean-Paul hebben;

use ModernWays;
select Voornaam, Familienaam, Titel 
	from Boeken
		where (Voornaam = 'Hugo') 
			or (Voornaam = 'Jean-Paul');

8. Selecteer alle boeken van de auteurs die de voornaam Hugo of Jean-Paul hebben en een boek in 1970 hebben geschreven;

use ModernWays;
select Voornaam, Familienaam, Titel, Verschijningsjaar 
	from Boeken
		where (Voornaam = 'Hugo' or Voornaam = 'Jean-Paul')
			and Verschijningsjaar = '1970';
			
9. Je hebt de familienaam Sarthe verkeerd geschreven het moet Sartre zijn, schrijf een update statement die die familienaam verbetert;

use ModernWays;
update Boeken
set Familienaam = 'Sarthre'
where Familienaam = 'Sarthe';

10. Voeg de categorie 'Literatuur' toe voor de boeken van Hugo Claus en Hugo Raes.

Use ModernWays;
update Boeken
   set Categorie = 'Literatuur'
   where (Familienaam = 'Claus' and Voornaam = 'Hugo') 
   or (Familienaam = 'Raes' and Voornaam = 'Hugo');


11. Voor het boek van Jean-Paul Sartre met de titel Het zijn en het niets voeg je de categorie 'Filosofie' toe.

Use ModernWays;
update Boeken
   set Categorie = 'Filosofie'
   where (Familienaam = 'Sartre' and Voornaam = 'Jean-Paul' and Titel = 'Het zijn en het niets');

12. Selecteer de boeken van Sartre met de categorie Filosofie en de boeken geschreven door Hugo Claus.

use ModernWays;
select Voornaam, Familienaam, Titel, Categorie, Verschijningsjaar from Boeken
    where (Familienaam = 'Sartre' and Categorie = 'Filosofie') 
    or (Voornaam = 'Hugo' and Familienaam = 'Claus');










