-- NB
-- 04 juni 2018
--
use ModernWays;
-- we gaan eerst een boek van een vrouw toevoegen
-- bestandnaam: BoekenNormalizeInsertOne.sql
--
-- auteur toevoegen
-- de Id van Mevrouw is 1, dat is de Id kolom van de Aanspreektitel tabel
insert into Personen (
   Voornaam, 
   Familienaam,
   IdAanspreekTitel
)
values (
   'Hilary', 
   'Mantel',
   1
);

select * from Personen;