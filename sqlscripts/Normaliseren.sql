-- NB
-- 02 mei 2018
-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
--
-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam nvarchar(255) not null,
    Familienaam nvarchar(255) not null
);


-- NB
-- 02 mei 2018
-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
--
use ModernWays;
insert into Personen
  select distinct Voornaam, Familienaam
    from Boeken;


Use ModernWays;
select Voornaam, Familienaam from Personen
	order by Voornaam, Familienaam