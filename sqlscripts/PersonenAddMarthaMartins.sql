-- NB
-- 16 mei 2018
-- PersonenAddMarthaMartins.sql

Use ModernWays;
insert into Personen (
    Voornaam,
    Familienaam,
    Aanspreektitel,
    Straat,
    Stad,
    Huisnummer,
    Biografie,
    Commentaar)
values (
    'Martha',
    'Martins',
    'Mevrouw' ,
    'Plopperdeplopstraat',
    'Mechelen',
    '23',
    'Ze leefde lang en gelukkig',
    'Goede Schrijver')