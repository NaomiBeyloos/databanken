Ik wil alle voornamen en namen van de schrijvers en geen dubbele. Voornaam en familienaam moeten verschillend zijn. De lijst staat in alfabetische volgorde, 
eerst Familienaam en dan Voornaam. Als je ORDER BY gebruikt moeten de kolommen in de order by clausule ook worden opgenomen na de DISTINCT clausule.


use ModernWays;
select distinct * 
from Boeken 
  order by Familienaam asc, Voornaam;

Ik wil alle voornamen en namen van de schrijvers en geen dubbele die de letter a en/of e in hun familienaam hebben staan. De lijst staat in alfabetische volgorde, 
eerst Familienaam en dan Voornaam.

use ModernWays;
select distinct * 
from Boeken
where Familienaam like '%a%' or Familienaam like '%e%'
  order by Familienaam asc, Voornaam;