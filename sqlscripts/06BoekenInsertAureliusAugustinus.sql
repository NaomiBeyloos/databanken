-- NB
-- 28 feb 2018
-- Werken met INSERT
-- Bestandsnaam: BoekenInsertAureliusAugustinus.sql
use ModernWays;
insert into Boeken(
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
values (
/*voornaam, familienaam, titel, stad, uitgeverij, verschijningsjaar, herdruk, commentaar, categorie, insertedbye*/
('John', 'Waters', 'Crackpot, the Obsessions of John Waters', '', 'Vintage', '1978', '2007', 'Gelezen', 'Humor', 'Naomi Beyloos'),
('Irvine', 'Welsh', 'Glue', '', 'Jonathan Cape', '2001', '', 'Gelezen', 'Roman', 'Naomi Beyloos'),
('Michael', 'Crichton', 'Jurassic Park', '', 'Alfred Knopf', '1990', '', 'Gelezen', 'Sci-Fi', 'Naomi Beyloos'),
('Michael', 'Crichton', 'Jurassic World', '', 'Alfred Knopf', '1995', '1996', 'Gelezen', 'Sci-Fi', 'Naomi Beyloos'),
('Aldous', 'Huxley', 'Brave New World', '', '', '1932', '', 'Gelezen', 'Sci-Fi', 'Naomi Beyloos'),
('James', 'Cain', 'The Postman Always Rings Twice', '', 'Alfred Knopf', '1934', '', 'Gelezen', 'Misdaad', 'Naomi Beyloos'),
('Paul', 'Theroux', 'The Mosquito Coast', '', 'Hamish Hamilton', '1981', '', 'Gelezen', 'Roman', 'Naomi Beyloos'),
('Paul', 'Theroux', 'O-Zone', '', 'Putman', '1986', '', 'Gelezen', 'Sci-Fi', 'Naomi Beyloos'),
('Nick', 'Cave', 'And the Ass Saw the Angel', '', 'Black Spring Press', '1989', '2007', 'Gelezen', 'Fictie', 'Naomi Beyloos'),
('Nick', 'Cave', 'Crackpot, the Obsessions of John Waters', '', 'Canongate Books', '2009', '', 'Gelezen', 'Fictie', 'Naomi Beyloos');
