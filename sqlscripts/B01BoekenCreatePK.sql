-- NB
-- 21 februari 2018
-- naam van scriptbestand is BoekenCreate
-- volgt het patroon Tabelnaam + DDL statement

USE ModernWays;

DROP TABLE IF EXISTS `Boeken`;

-- de naam van de tabel in Pascalnotatie
CREATE TABLE Boeken(
    Voornaam NVARCHAR(50),
    Familienaam NVARCHAR(80),
    Titel NVARCHAR(255),
    Stad NVARCHAR(50),
    -- alleen het jaartal, geen datetime
    -- omdat de kleinste datum daarin 1753 is
    -- varchar omdat we ook jaartallen kleiner dan 1000 hebben
    Verschijningsjaar CHAR(4),
    Uitgeverij NVARCHAR(80),
    Herdruk CHAR(4),
    Commentaar nvarchar(2000),
    Categorie nvarchar(120),
    InsertedBy nvarchar(255),
    Id int auto_increment not null,
    constraint pk_Boeken_Id primary key(Id)
    
);