Alle boeken die niet zijn geschreven door een auteur waarvan de familienaam begint met een d, k of r en die gepubliceerd zijn in de 
jaren zeventig en twintig van de vorige eeuw.

use ModernWays;
select Familienaam, Titel, Verschijningsjaar
from Boeken
where not (Familienaam like 'D%' or Familienaam like 'K%' or Familienaam like 'R%')
and (Verschijningsjaar like '192_' or Verschijningsjaar like '199_' );


Alle boeken geschreven door de auteurs waarvan de familienaam begint met een d, k of r en die een boek gepubliceerd hebben in het tweede 
jaar van elk decennium van de vorige eeuw.

use ModernWays;
select Familienaam, Titel, Verschijningsjaar
from Boeken
where(Familienaam like 'D%' or Familienaam like 'K%' or Familienaam like 'R%')
and (Verschijningsjaar like '19_2');