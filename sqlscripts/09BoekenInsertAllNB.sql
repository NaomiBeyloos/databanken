-- NB
-- 22/04/2018
-- Bestandsnaam: 09BoekenInsertAllNB.sql

use ModernWays;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
-- Voornaam, Familienaam, Titel, Stad, Uitgeverij, Verschijningsjaar, Herdruk, Commentaar, Categorie, InsertedBy
values 
('Nick', 'Cave', 'And the Ass Saw the Angel', '', 'Black Spring Press', '1998', '', 'Gelezen', 'Roman', 'NB'),
('Nick', 'Cave', 'Lees wijzer bij Zijn en Tijd', '', 'Canongate Books', '2009', '', 'Gelezen', 'Roman', 'NB'),
('Paul', 'Theroux', 'O-Zone', '', 'Putman', '1986', '', 'Gelezen','Sci-Fi', 'NB'),
('Paul', 'Theroux', 'The Mosquito Coast', '', 'Hamish Hamilton', '1981', '', 'Gelezen', 'Roman', 'NB'),
('Michael', 'Crichton', 'Jurassic Park', '', 'Alfred A. Knopf', '1990', '', 'Gelezen','Sci-Fi', 'NB'),
('Michael', 'Crichton', 'The Lost World', '', 'Alfred A. Knopf', '1995', '?', 'Gelezen', 'Sci-Fi', 'NB'),
('John', 'Waters', 'Crackpot, the Obsessions of John Waters', '', 'Vintage', '1989', '?', 'Gelezen','Humor', 'NB'),
('James M', 'Cain', 'The Postman Always Rings Twice', '', 'Alfred A. Knopf', '1934', '', 'Gelezen','Misdaad', 'NB'),
('Charles', 'Bukowski', 'Factotum', '', 'Black Sparrow Books', '1975', '2005', 'Gelezen', 'Autobiografie','NB'),
('Irvine', 'Welsh', 'Glue', '', 'Jonathan Cape', '2001', '', 'Gelezen', 'Roman', 'NB');
