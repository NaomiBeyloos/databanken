-- NB
-- 28 feb 2018
-- Werken met INSERT
-- Bestandsnaam: BoekenInsertStevenSherill.sql
--
use ModernWays;
insert into Boeken(
   Voornaam,
   Familienaam,
   Titel,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Categorie,
   InsertedBy
)
values (
   'Steven',
   'Sherrill',
   'The Minotaur Takes a Cigarette Break',
   'Blair',
   '2003',
   '2016',
   'Roman',
   'Naomi Beyloos'
);