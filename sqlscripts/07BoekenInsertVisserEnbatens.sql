-- NB
-- 28 feb 2018
-- BoekenInsertVisserEnbatens.sql
-- 

use ModernWays;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
values
('Gerard', 'Visser', 'Heideggers vraag naar de techniek', 'Nijwegen', '', '2014', '', '', '', ''),
('Diderik', 'Batens', 'Logicaboek', '', 'Garant', '1999', '', '', '', '');