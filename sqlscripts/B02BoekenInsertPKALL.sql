-- NB
-- 14 april 2018
-- Werken met INSERT

use ModernWays;
insert into Boeken(
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
values (
   'Alfred',
   'Denker',
   'Onderweg in Zijn en Tijd',
   '',
   'Damon',
   '2017',
   '',
   'Geen commentaar',
   'Filosofie',
   'JI'
),
(
   'Aurelius',
   'Augustinus',
   'De stad van God',
   'Baarn',
   'Uitgeverij Baarn',
   '1983',
   '1992',
   'Nog te lezen',
   'Theologie',
   'JI'
),
('Gerard', 'Visser', 'Heideggers vraag naar de techniek', 'Nijwegen', '', '2014', '', '', 'Filosofie', 'JI'),
('Diderik', 'Batens', 'Logicaboek', '', 'Garant', '1999', '', '', 'Filosofie', 'JI'),
('Samuel', 'Ijsseling', 'Heidegger. Denken en Zijn. Geven en Danken', 'Amsterdagm', '', '2014', '', 'Nog te lezen', 'Filosofie', 'JI'),
('Jacob', 'Van Sluis', 'Lees wijzer bij Zijn en Tijd', '', 'Budel', '1998', '', 'Goed boek', 'Filosofie', 'JI'),
('Emile', 'Benveniste', 'Le vocabulaire des institutions Indo-Européennes. 2. Pouvoir droit religion', 'Paris', 'Les ditions de minuit', '1969', '', 'Een goed geschiedenis boek','Linguistiek', 'JI'),
('Evert W.', 'Beth', 'De Wijsbegeerte der Wiskunde. Van Parmenides tot Bolzano', 'Antwerpen', 'Philosophische Biliotheek Uitgeversmij. N.V. Standaard-Boekhandel', '1944', '?', 'Een goed boek', 'Filosofie', 'JI'),
('Evert W.', 'Beth', 'Wijsbegeerte der Wiskunde', 'Antwerpen', 'Philosophische Biliotheek Uitgeversmij. N.V. Standaard-Boekhandel', '1948', '?', 'Een goed boek','Wiskunde', 'JI'),
('Rémy', 'Bernard', 'Antonin le Pieux. Le siècle d''or de Rome 138-161', '?', 'Librairie Arthme Fayard', '2005', '', 'Een goed boek', 'Geschiedenis', 'JI'),
('Marc', 'Bloch', 'Rois et serfs et autres écrits sur le servage', 'Paris', 'La boutique de l''histoire', '1996', '', 'Een goed boek','Geschiedenis', 'JI'),
('Pierre', 'Bonte en Michel Izard', 'Dictionnaire de l''etnologie et de l''anthropologie', '?', 'PUF', '1991', '', 'Een goed boek','Anthropologie', 'JI'),
('Robert', 'Bly', 'The sibling society', 'Londen', 'Persus', '1996', '?', 'Een interessant boek', 'Antropologie','JI'),
('Fernand', 'Braudel', 'De middellandse zee. Het landschap en de mens', 'Amsterdam/Antwerpen', 'Uitgeverij Contanct', '1992', '?', 'Uit het Frans vertaald: La méditerranée. La part du milieu. Parijs: Librairie Armand Colin, 1966', 'Geschiedenis', 'JI'),
('Timothy', 'Gowers', 'Wiskunde, de kortste introductie', 'Utrecht', 'Uitgeverij Het Spectrum B.V.', '2003', '', 'Oorpronkelijke titel: Mathematics a very schort introduction. Oxford University Press, 2002', 'Wiskunde', 'JI'),
('Emile', 'Benveniste', 'Le vocabulaire des institutions Indo-Européennes. 1. économie, parenté, société', 'Paris', 'Les Éditions de minuit', '1969', '', 'Een goed geschiedenis boek','Linguistiek', 'JI'),
('Marc', 'Bloch', 'Apologie pour l''histoire ou métier d''historien', 'Paris', 'Armand Colin', '1949', '2018', 'Tweede uitgave','Geschiedenis', 'JI'),
('Fernand', 'Braudel', 'L''identité de la France', 'Paris', 'Arthaud', '1992', '', 'C''est l''oeuvre finale de l''un des plus importants historiens français du XXe siècle.', 'Geschiedenis', 'JI'),
('Fernand', 'Braudel', 'Civilisation matérielle, économie et capitalisme, XVe-XVIIIe siècle, Tome 1 : Les Structures du Quotidien', 'Paris', 'Armand Colin', '1986', '', 'Belangrijk boek, deel 1 van 3 delen', 'Geschiedenis', 'JI'),
('Fernand', 'Braudel', 'Civilisation matérielle, économie et capitalisme, XVe-XVIIIe siècle, Tome 2 : Les Jeux de l''échange', 'Paris', 'Armand Colin', '1986', '', 'Belangrijk boek, deel 2 van 3 delen', 'Geschiedenis', 'JI'),
('Fernand', 'Braudel', 'Civilisation matérielle, économie et capitalisme, XVe-XVIIIe siècle, Tome 3 : Le temps du monde', 'Paris', 'Armand Colin', '1986', '', 'Belangrijk boek, deel 3 van 3 delen', 'Geschiedenis', 'JI');



--Use ModernWays;
--select * from Boeken;