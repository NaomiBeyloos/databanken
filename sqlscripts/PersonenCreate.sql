Use ModernWays;
create table Personen(
	Id int auto_increment,
	Voornaam nvarchar (255) not null,
	Familienaam nvarchar (255) not null,
	Leeftijd int,
	constraint pk_Personen_Id primary key(Id)
    );
    
    
--Use ModernWays;
--insert into Personen(
--    Voornaam,
--   Familienaam,
--   Leeftijd)
--values ('Sarah', 'Coppens', 30),
--	   ('Jan', 'Desomer', 30);

--Use ModernWays;
--insert into Personen(
--    Voornaam,
--    Familienaam,
--    Leeftijd)
--    values ('Yves', 'Coppens', 81);

--Use ModernWays;
--update Personen set Leeftijd=83
--	where Id = 3;


--Use ModernWays;
--select * from Personen where Id = 3

--Use ModernWays;
--create table Personen2 (
--    Voornaam nvarchar (255),
--    Familienaam nvarchar (255),
--    Code char (4),
--    constraint pk_Personen2_Code primary key (Code)
--    );
-- zonder auto incriment, geen automatische nummering

--Use ModernWays;
--insert into Personen2(
--    Voornaam,
--    Familienaam,
--    Code)
--values
--    ('Sofie', 'Lanen', 'AAAA'),
--    ('Abdel', 'El Fahrisi', 'BAAA')
-- zonder auto incriment, geen automatische nummering, eigen code moeten geven

--Use ModernWays;
--insert into Personen2(
--    Voornaam,
--    Familienaam,
--   Code)
--values
--    ('Fatima', 'El Fahrisi', 'CAAA'),
--	('Bart', 'Lanen', 'DAAA')
-- zonder auto incriment, geen automatische nummering, eigen code moeten geven