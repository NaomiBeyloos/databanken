Selecteer de voornaam, familienaam, titel uit de tabel Boeken die gepubliceerd werden tussen 1998 en 2001.

use ModernWays;
select Voornaam, Familienaam, Titel
  from Boeken
  where Verschijningsjaar between '1998' and '2001';
  
Selecteer de voornaam, familienaam, titel uit de tabel Boeken die gepubliceerd werden tussen 1900 en 2011 en waarvan de familienaam 
begint met de letter B. Orden de titels alfabetisch.
  
use ModernWays;
select Voornaam, Familienaam, Titel
  from Boeken
  where Verschijningsjaar between '1998' and '2001'
  or Familienaam like 'b%'
  order by Titel asc, Familienaam, Voornaam;


Selecteer de voornaam, familienaam, titel en verschijningsdatum uit de tabel Boeken die gepubliceerd werden tussen 1900 en 2011 en 
waarvan de familienaam de letters B, F of A bevatten. Orden de boeken op verschijningsdatum.

use ModernWays;
select Voornaam, Familienaam, Titel, Verschijningsjaar
  from Boeken
  where Verschijningsjaar between '1998' and '2001'
  or (Familienaam like '%a%' or Familienaam like '%b%'or Familienaam like '%f%')
  order by Verschijningsjaar asc, Familienaam, Voornaam, Titel;


Selecteer de voornaam, familienaam, titel en verschijningsdatum uit de tabel Boeken die gepubliceerd werden na 1990. Orden de boeken op verschijningsdatum.

use ModernWays;
select Voornaam, Familienaam, Titel, Verschijningsjaar
  from Boeken
  where Verschijningsjaar between '1991' and '2018'
  order by Verschijningsjaar asc, Familienaam, Voornaam, Titel;
  
  of
  
use ModernWays;
select Voornaam, Familienaam, Titel, Verschijningsjaar
  from Boeken
  where Verschijningsjaar like '199_' or Verschijningsjaar like '20__'
  order by Verschijningsjaar asc, Familienaam, Voornaam, Titel;

Selecteer de voornaam, familienaam, titel en verschijningsdatum uit de tabel Boeken die gepubliceerd werden voor 2010. Orden de boeken op familienaam en voornaam.

use ModernWays;
select Voornaam, Familienaam, Titel, Verschijningsjaar
  from Boeken
  where Verschijningsjaar between '1000' and '2009'
  order by Familienaam asc, Voornaam asc;
  


 