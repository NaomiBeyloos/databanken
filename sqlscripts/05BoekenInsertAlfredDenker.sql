-- NB
-- 28 feb 2018
-- Bestandsnaam: BoekenInsertAlfredDenker.sql
-- 
use ModernWays;
insert into Boeken (
    Voornaam,
    Familienaam,
    Titel,
    Uitgeverij,
    Verschijningsjaar,
    InsertedBy
)  
 
values(
     
     'Alfred',
     'Denker',
     'Onderweg in Zijn en Tijd',
     'Damon',
     '1017',
     'Naomi Beyloos'
     );