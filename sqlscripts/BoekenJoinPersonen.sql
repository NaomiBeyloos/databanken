-- NB
-- 16 mei 2018
-- Join.sql


Use ModernWays;
select Personen.Voornaam, 
	   Personen.Familienaam, 
	   Boeken.Titel
from Boeken
	join Personen
	on Boeken.IdAuteur = Personen.Id;
	
-- inner join
-- voor de gebruiker, zodat zij 1 tabel zien


-- Use ModernWays;
--Select Personen.Voornaam,
--	   Personen.Familie,
--	   Boeken.Titel
--from Boeken
--inner join Personen
--on Boeken.IdAuteur = Personen.Id
--order by Personen.Familienaam desc;


--use ModernWays;
--select Voornaam, Familienaam, Titel 
--	from Personen
--	inner join Boeken
--	on Personen.Id = Boeken.IdAuteur
--	order by Voornaam, Familienaam;
	

--use ModernWays;
--select Voornaam, Familienaam, Titel 
--	from Personen
--	left join Boeken
--	on Personen.Id = Boeken.IdAuteur
--	order by Voornaam, Familienaam;



-- NB
-- 23 mei 2018
-- Join.sql

--use ModernWays;
--select Voornaam, Familienaam, Titel 
--	from Personen
--	right join Boeken
--	on Personen.Id = Boeken.IdAuteur
--	order by Voornaam, Familienaam;


--use ModernWays;
--select Voornaam, Familienaam, coalesce (Titel, 'Heeft geen boek geschreven') 
--	from Boeken
--	right join Personen
--	on Personen.Id = Boeken.IdAuteur
--	order by Voornaam, Familienaam;

--use ModernWays;
--select Voornaam, Familienaam, coalesce (Titel, 'Heeft geen boek geschreven') as 'Titel boek'
--	from Boeken
--	right join Personen
--	on Personen.Id = Boeken.IdAuteur
--	order by Voornaam, Familienaam;

--use ModernWays;
--select Voornaam, Familienaam, coalesce (Titel, 'Heeft geen boek geschreven') as 'Titel boek'
--	from Personen
--	left join Boeken
--	on Personen.Id = Boeken.IdAuteur
--where Boeken.IdAuteur is null
--	order by Voornaam, Familienaam;


--use ModernWays;
--update Personen 
--set Familienaam = 'Mei Chang' 
--where Id <=2;

use ModernWays;
update Personen 
set Stad = 'Mechelen' 
where Id <= 2;

select * from Personen




use ModernWays;
update Personen 
set Familienaam = 'Augustinus' 
where Id =2;

select * from Personen


use ModernWays;
update Personen 
set Familienaam = 'Denkers' 
where Id = 1;

select * from Personen


use ModernWays;
update Personen 
set Stad = 'Kontich' where Id = 1;

use ModernWays;
update Personen 
set Stad = 'Kontich' where Id = 2;


use ModernWays;
update Personen 
set Stad = 'Rumst' where Id >2;

use ModernWays;
update Personen 
set Stad = 'Gent' where Id >4;

use ModernWays;
update Personen 
set Stad = 'Oostende' where Id >6;

use ModernWays;
update Personen 
set Stad = 'Ieper' where Id >8;

use ModernWays;
update Personen 
set Stad = 'Brugge' where Id >10;

use ModernWays;
update Personen 
set Stad = 'De Panne' where Id >12;

use ModernWays;
update Personen 
set Stad = 'Mortsel' where Id >14;

use ModernWays;
update Personen 
set Stad = 'Brasschaat' where Id >16;

use ModernWays;
update Personen 
set Stad = 'Ekeren' where Id >18;

use ModernWays;
update Personen 
set Stad = 'Machelen' where Id >20;

use ModernWays;
update Personen 
set Stad = 'Walem' where Id >22;

use ModernWays;
update Personen 
set Stad = 'Mechelen' where Id =23;

select * from Personen




--Use ModernWays;
--update Personen
--	set Aanspreektitel = 'Mijnheer'
--	where Id <16;

--select * from Personen;

--Use ModernWays;
--alter table Personen modify column Aanspreektitel nvarchar (40);




--Use ModernWays;
--update Personen
--	set Aanspreektitel = 'Mijnheer'
--	where Id <21;

--select * from Personen;

