-- NB
-- 30 april 2018
-- naam van scriptbestand is WinkelCreate
-- volgt het patroon Tabelnaam + DDL statement


USE ModernWays;
DROP TABLE IF EXISTS `Winkels`;



-- de naam van de tabel in Pascalnotatie
CREATE TABLE Winkels(
    Naam NVARCHAR(255),
    Adres NVARCHAR(255),
    Postcode CHAR(4),
    Stad NVARCHAR(50),
    Telefoon NVARCHAR(50),
    -- hoeveel tekens heeft een vaste nummer?
    Id int auto_increment,
    constraint pk_Winkels_Id primary key(id)
    -- idd niet toevoegen bij importeren en dan kolomnamen toevoegen, telefoon is de laatste kollom die je toevoegt

);


ALTER TABLE Winkels AUTO_INCREMENT = 1;
-- de id teller begint bij 1, bij ALTER TABLE Winkels AUTO_INCREMENT = 6; begint de idteller bij 6 en gaat zo verder naar 7, 8, 9;